package main

import (
	"flag"
	"math/rand"
	"time"
)

var entering []bool
var number []int
var n int

func lock(i int) {
	entering[i] = true
	number[i] = 1 + max()
	entering[i] = false

	for j := 0; j < n; j++ {
		for entering[j] {
			time.Sleep(time.Millisecond)
		}
		for number[j] != 0 && inLexOrder(j, i) {
			time.Sleep(time.Millisecond)
		}
	}
}

func unlock(i int) {
	number[i] = 0
}

func max() int {
	max := number[0]
	for i := 1; i < n; i++ {
		if number[i] > max {
			max = number[i]
		}
	}
	return max
}

func inLexOrder(j int, i int) bool {
	return number[j] < number[i] || (number[j] == number[i] && j < i)
}

func thread(i int) {
	length := time.Duration(i)
	for {
		println("Thread ", i, " wants to start operation which takes ", length, " seconds")
		lock(i)
		println("Thread ", i, " starts operation which takes ", length, " seconds")
		time.Sleep(time.Second * length)
		length = time.Duration(rand.Intn(5-1) + 1)
		println("Thread ", i, " finishes operation")
		unlock(i)
	}
}

func main() {
	nPtr := flag.Int("n", 0, "number of processes")
	flag.Parse()
	n = *nPtr

	rand.Seed(time.Now().UTC().UnixNano())

	entering = make([]bool, n)
	number = make([]int, n)

	for i := 0; i < n; i++ {
		go thread(i)
	}

	for {
	}

}
